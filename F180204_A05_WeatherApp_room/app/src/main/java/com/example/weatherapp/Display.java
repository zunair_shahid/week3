package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Date;


public class Display extends AppCompatActivity {
    String City;
    Button save;
    Button fetch;
    String url = "http://api.weatherstack.com/current";
    String key = "09eabf22ef28349d80d6a879778cb186";
    String req1 = "";
    TextView locate;
    TextView wDesc;
    TextView tempe;
    TextView windv;
    TextView precv;
    TextView presv;
    TextView feel;

    String cityName,countryName,desc,wind;
    double temp;



    DecimalFormat df = new DecimalFormat("#.##");

    //"http://api.weatherstack.com/current?access_key=09eabf22ef28349d80d6a879778cb186&query=New%20York"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        Intent intent = getIntent();
        City = intent.getStringExtra("City");

        save = findViewById(R.id.save);
        fetch = findViewById(R.id.fetch);

        locate =findViewById(R.id.locaw);
        wDesc =findViewById(R.id.descw);
        tempe =findViewById(R.id.tempw);
        windv =findViewById(R.id.windw);
        precv =findViewById(R.id.precew);
        presv =findViewById(R.id.presw);
        feel = findViewById(R.id.feelw);



        req1 = url + "?access_key=" + key + "&query=" + City;


        getData();



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new bgthread().start();
            }
        });

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Room_Int = new Intent(Display.this, RoomData.class);
                startActivity(Room_Int);
                finish();
            }
        });

    }


    Response.Listener<JSONObject> successBLock = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {

            try {
                JSONObject jsonResponse = response;
                JSONObject current = jsonResponse.getJSONObject("current");
                JSONObject location = jsonResponse.getJSONObject("location");
                JSONArray des = current.getJSONArray("weather_descriptions");
                desc = des.getString(0);
                temp = current.getDouble("temperature");
                double feelsLike = current.getDouble("feelslike");
                float pressure = current.getInt("pressure");
                int preci = current.getInt("precip");
                wind = current.getString("wind_speed");
                countryName = location.getString("country");
                cityName = location.getString("name");
                locate.setText(cityName+", "+countryName);
                tempe.setText(df.format(temp) + " °C");
                wDesc.setText(desc);
                windv.setText("Wind: " + wind + "kmph");
                presv.setText(" Pressure: " + pressure +  "mb");
                presv.setText("Precip: "+ preci);
                feel.setText("Feels Like: " + df.format(feelsLike) + " °C");

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(Display.this, "No data", Toast.LENGTH_SHORT).show();
            }
        }
    };
    Response.ErrorListener failureBlock = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(Display.this,error.toString(), Toast.LENGTH_SHORT).show();
        }


    };
    private void getData()
    {
        String link = req1;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,link,null,successBLock,failureBlock);
        Volley.newRequestQueue(this).add(request);
    }

    class bgthread extends Thread
    {
        public void run()
        {
            super.run();
            WeatherDatabase db = Room.databaseBuilder(getApplicationContext(),WeatherDatabase.class,"room_db").build();
            WeatherDAO weatherDAO = db.weatherDAO();
            weatherDAO.insertWeather(new Weather(cityName,countryName,temp,desc,wind,"NS"));
        }
    }

}
