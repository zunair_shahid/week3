package com.example.assignment_02;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
public class MainActivity extends AppCompatActivity {

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            String name="f180204@nu.edu.pk";
            String password="12";

            EditText email = findViewById(R.id.email);
            EditText pass = findViewById(R.id.password);
            Button button =findViewById(R.id.loginbtn);
            Intent intent = new Intent(this, Profile.class);

            button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals(name)&& pass.getText().toString().equals(password)) {
                    Toast.makeText(MainActivity.this,"Success! Logged In",Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Your Email or Password does not match");
                    builder.setTitle("Failed!");
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }


    public void Signup(View view) {
        startActivity(new Intent(MainActivity.this,SignUP.class));
    }
}